# User space block backend (us-blkback)
This provides a completely userspace block backend for Xen guests that utilize
the blkfront driver (winvbd or xen-blkfront on Windows and Linux respectively). The main target of this driver is for use with the Xen compatibility layer on top of a flavor of the Bareflank hypervisor, however main development was done against Xen 4.9 and should work on pretty much any modern Xen.

# Building the user space block backend
The user space block backend builds on both Windows and Linux.

## Windows
us-blkback has 4 other major dependencies required to build it:
* cmake
* Git for Windows
* Xen Windows PV drivers (windows-pv-drivers.git)
* libxenbe (libxenbe.git, Windows branch)

Open git bash and enter the following:

```
cd /c/Users/user/Documents
git clone -b windows https://github.com/brendank310/libxenbe.git
git clone https://gitlab.com/brendank310/us-blkback.git
git clone https://gitlab.com/redfield/winpv/windows-pv-drivers.git
```

### Resolve the windows-pv-driver dependency
Run the provisioning script described in the windows-pv-drivers repository README.md (https://gitlab.com/redfield/winpv/windows-pv-drivers/-/blob/master/README.md), and follow the building instructions.

### Resolve the libxenbe dependency
1) Create a folder within the libxenbe folder (C:\Users\user\Documents\libxenbe), called build.
2) Open a PowerShell prompt and navigate to the above build directory (C:\Users\user\Documents\libxenbe\build).
3) Enter the following command:
```
cmake -G "Visual Studio 15" -DXENIFACE_WINDOWS_INCLUDE_PATH='C:\Users\user\Documents\windows-pv-drivers\xeniface\include' -DXENBUS_WINDOWS_INCLUDE_PATH='C:\Users\user\Documents\windows-pv-drivers\xenbus\include' -DXENIFACE_WINDOWS_LIB_PATH='C:\Users\user\Documents\windows-pv-drivers\xeniface\vs2017\x64\Windows10Debug' -DXENBUS_WINDOWS_LIB_PATH='C:\Users\user\Documents\windows-pv-drivers\xenbus\vs2017\Windows10Debug\x64' -DWITH_WIN=ON -DWITH_TEST=OFF -A x64 ..
```
4) Navigate to the build folder and open the libxenbe.sln in Visual Studio 2017.
5) Scope to the xenbe project.
6) Choose build project.

### Build the us-blkback binary
1) Create a folder within the us-blkback folder (C:\Users\user\Documents\us-blkback), called build.
2) Open a PowerShell prompt and navigate to the above build directory (C:\Users\user\Documents\us-blkback\build).
3) Enter the following command:
```
cmake -G "Visual Studio 15" -DXENVBD_INCLUDE_PATH='C:\Users\user\Documents\windows-pv-drivers\xenvbd\include' -DXENIFACE_INCLUDE_PATH='C:\Users\user\Documents\windows-pv-drivers\xeniface\include' -DXENBUS_INCLUDE_PATH='C:\Users\user\Documents\windows-pv-drivers\xenbus\include' -DXENIFACE_LIB_PATH='C:\Users\user\Documents\windows-pv-drivers\xeniface\vs2017\x64\Windows10Debug' -DXENBUS_LIB_PATH='C:\Users\user\Documents\windows-pv-drivers\xenbus\vs2017\Windows10Debug\x64' -DLIBXENBE_INCLUDE_PATH='C:\Users\user\Documents\libxenbe\include' -DLIBXENBE_LIB_PATH='C:\Users\user\Documents\libxenbe\build\src\Debug' -DWITH_WIN=ON -A x64 ..
```
4) Navigate to the build folder in the file explorer and open the us-blkback.sln in Visual Studio 2017.
5) Select Build All.

## Linux
